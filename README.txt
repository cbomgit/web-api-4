To install Node app: 

$ git clone <repo url>
$ npm install
$ node index.js

Base URL: 
https://webapicbom-test.apigee.net

Application routes: 

GET /movies - retrieves all movies

GET /movies/<movie name> - retrieves movies matching name in parameter

POST /movies - creates a new movie, the following body is expected: 

   title - title of the movie
   date_released - release date of movie
   actors - array of length 3 for actors in each movie

DELETE /movies/<movie name> delete the specified movie
